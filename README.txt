This directory contains simple scripts to set up and run two virtual
machines for an NFS server and an NFS client. The two machines are
connected by a virtual network (using QEMU's "socket" networking)
and NFS is secured by Kerberos.

There seems to be a problem in the Linux NFS client when using
NFSv4.0 + Kerberos which causes blocked tasks on the client
machine. The scripts are intended to make reproducing the problem
simple by setting up an NFS server and an NFS client in a
configuration that is likely to exhibit the problem.

To set up the two virtual machines, run

  $ ./build.sh

as user "root". The script uses "debootstrap" to set up two small
Debian 9 systems in the directories "client" and "server",
respectively. Both systems are configured to have users "root" and
"user", both with password "pass".

To start the virtual machines, run (as "root")

  $ ./server.sh

and

  $ ./client.sh

The virtual machines use the 9p protocol to mount their root
filesystems from the directories "server" and "client",
respectively. The serial console of each machine is connected
to the terminal.

The server exports its /dev/shm filesystem via NFS and the client
mounts it at /mnt (see /etc/fstab).

To reproduce the Kerberos+NFSv4.0-related blocked tasks, log in to the
client as user "user" (with password "pass"), run

  $ kinit

(and enter the password "pass" again), and run

  $ /hang.sh

The script /hang.sh calls the binary /writesync (source code is in
writesync.c) with parameters that seem to be likely to produce the
hangs quickly. After some time (usually less than 1000 iterations,
often only a few), the process "writesync" is reported by the kernel
as a blocked task ("INFO: task writesync:319 blocked for more than 120
seconds"). With recent kernels (e.g., 4.17-rc1), it also happens that the
"writesync" process hangs and the memory consumption increases rapidly
until the OOM killer has terminated all processes (and the kernel panics).

The problem seems to affect Linux kernels from 3.x up to 4.17-rc1.  It
seems that the problem does not occur when mounting with NFS versions
4.1 or 4.2, instead of 4.0. The problem can also be observed with
Solaris/Nexenta as the NFS server. The problem also seems to be
timing-sensitive. Slowing down the machines makes the problem go away,
e.g. when one runs QEMU without "-enable-kvm", or turns on function
tracing in the kernel, the client does not hang (but runs much
slower).

Typical kernel messages when the problem occurs:

[  194.793319] nfs: server server not responding, still trying
[  194.796285] nfs: server server not responding, still trying
[  194.797356] nfs: server server not responding, still trying
[  194.798233] nfs: server server not responding, still trying
[  242.656213] INFO: task writesync:370 blocked for more than 120 seconds.
[  242.660317]       Not tainted 4.16.0-rc4 #23
[  242.662931] "echo 0 > /proc/sys/kernel/hung_task_timeout_secs" disables this message.
[  242.667665] writesync       D    0   370    369 0x00000000
[  242.669848] Call Trace:
[  242.670184]  __schedule+0x292/0x840
[  242.670803]  schedule+0x2c/0x80
[  242.671311]  io_schedule+0x16/0x40
[  242.671863]  wait_on_page_bit_common+0x105/0x190
[  242.672644]  ? page_cache_tree_insert+0xe0/0xe0
[  242.673455]  __filemap_fdatawait_range+0xf6/0x160
[  242.674098]  filemap_write_and_wait_range+0x4b/0x90
[  242.674624]  nfs_file_fsync+0x46/0x200 [nfs]
[  242.675080]  vfs_fsync_range+0x51/0xb0
[  242.675480]  do_fsync+0x3d/0x70
[  242.675820]  SyS_fsync+0x10/0x20
[  242.676191]  do_syscall_64+0x73/0x130
[  242.676594]  entry_SYSCALL_64_after_hwframe+0x3d/0xa2
