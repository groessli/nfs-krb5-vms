#!/bin/bash

set -e

source defs.sh

if [ "`id -u`" -ne 0 ]; then
    echo "Must be root to build the machines."
    exit 1
fi

if [ ! -x "${DEBOOTSTRAP}" ]; then
    echo "${DEBOOTSTRAP} is not available (modify defs.sh if installed in other location)."
    exit 1
fi

export LANG=C.UTF-8

if [ ! -d "${SERVERROOT}" ]; then
    mkdir -p "${SERVERROOT}"
    "${DEBOOTSTRAP}" "${RELEASE}" "${SERVERROOT}"
fi

cp "${PRESEED}" "${SERVERROOT}/preseed"

# prepare the server directory with the setup common to both client and server
chroot "${SERVERROOT}" /bin/sh <<EOF
mount -t proc proc /proc
apt-get update
apt-get upgrade --yes
echo 'APT::Install-Recommends "0";' >/etc/apt/apt.conf.d/99no-recommends
apt-get install --yes linux-image-amd64 debconf-utils sudo tmux nfs-common
cat >>/etc/initramfs-tools/modules <<EFO
9p
9pnet
9pnet_virtio
EFO
update-initramfs -u
debconf-set-selections preseed
apt-get install --yes krb5-user
echo "LANG=C.UTF-8" >/etc/default/locale
echo "10.11.12.1  server.realm server" >>/etc/hosts
echo "10.11.12.2  client.realm client" >>/etc/hosts
adduser --disabled-password user
adduser user sudo
echo user:pass | chpasswd
echo root:pass | chpasswd
cat >/etc/krb5.conf <<EFO
[libdefaults]
  default_realm = REALM
[realms]
  REALM = {
    kdc = server.realm
    admin_server = server.realm
  }
[domain_realms]
  realm = REALM
  .realm = REALM
EFO
umount /proc
EOF

# copy server directory to client
if [ ! -d "${CLIENTROOT}" ]; then
    cp -a "${SERVERROOT}" "${CLIENTROOT}"
fi

# server-specific configuration
chroot "${SERVERROOT}" /bin/sh <<EOF
mount -t proc proc /proc
echo server >/etc/hostname
cat > /usr/sbin/policy-rc.d <<EFO
#!/bin/sh
exit 101
EFO
chmod 755 /usr/sbin/policy-rc.d
apt-get install --yes krb5-kdc krb5-admin-server nfs-kernel-server
(echo ; echo) | kdb5_util create -s
kadmin.local addprinc -pw pass user
kadmin.local addprinc -randkey nfs/server.realm@REALM
kadmin.local ktadd nfs/server.realm@REALM
kadmin.local addprinc -randkey nfs/client.realm@REALM
kadmin.local ktadd -k /client.keytab nfs/client.realm@REALM
cat >>/etc/network/interfaces <<EFO
auto ens3
iface ens3 inet static
  address 10.11.12.1
  netmask 255.255.255.0
EFO
echo "/dev/shm  10.11.12.0/24(fsid=0,rw,sec=krb5p,secure,no_subtree_check,no_all_squash)" >/etc/exports
rm /usr/sbin/policy-rc.d
umount /proc
EOF

# client-specific configuration
mv "${SERVERROOT}/client.keytab" "${CLIENTROOT}"
gcc -Wall -o "${CLIENTROOT}/writesync" writesync.c
echo -e "#!/bin/sh\n/writesync /mnt/file 512 13717" >"${CLIENTROOT}/hang.sh"
chmod 755 "${CLIENTROOT}/writesync" "${CLIENTROOT}/hang.sh"

chroot "${CLIENTROOT}" /bin/sh <<EOF
mount -t proc proc /proc
echo client >/etc/hostname
mv /client.keytab /etc/krb5.keytab
cat >>/etc/network/interfaces <<EFO
auto ens3
iface ens3 inet static
  address 10.11.12.2
  netmask 255.255.255.0
EFO
echo "server:/  /mnt  nfs  vers=4.0,sec=krb5p  0 0" >>/etc/fstab
umount /proc
EOF

echo "Setup finished"
