RELEASE=stretch

DEBOOTSTRAP=/usr/sbin/debootstrap
QEMU=/usr/bin/qemu-system-x86_64
BASEDIR="`dirname $0`"
SERVERROOT="${BASEDIR}/server"
CLIENTROOT="${BASEDIR}/client"
PRESEED="${BASEDIR}/preseed"

function startvm {
    if [ ! -x "${QEMU}" ]; then
	echo "${QEMU} is not available (modify defs.sh if installed in other location)"
	exit 1;
    fi
    root="$1"
    kernel="${root}/vmlinuz"
    initrd="${root}/initrd.img"
    netaction="$2"
    if [ "${netaction}" = "listen" ]; then
	mac=52:54:00:12:34:56
    else
	mac=52:54:00:12:34:57
    fi
    "${QEMU}" -enable-kvm -m 2048 -nographic \
       -net nic,macaddr="${mac}" -net socket,"${netaction}"=127.0.0.1:1234 \
       -kernel "${kernel}" -initrd "${initrd}" \
       -virtfs local,id=fs0,path="${root}",security_model=passthrough,mount_tag=root9p \
       -append "init=/sbin/init console=ttyS0,38400,n,8,1 rw root=root9p rootfstype=9p rootflags=trans=virtio"
}

