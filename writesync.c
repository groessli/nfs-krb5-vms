#include <fcntl.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    int fd;

    if (argc != 4) {
        fprintf(stderr, "USAGE: %s filename stride N\n", argv[0]);
        exit(1);
    }

    const int stride = atoi(argv[2]);
    const int N = atoi(argv[3]);

    for (int count=1; ; ++count) {
        fprintf(stdout, "%d\n", count);
        fflush(stdout);

        fd = open(argv[1], O_CREAT|O_TRUNC|O_WRONLY, 0600);
        if (fd == -1) {
            perror("could not open file");
            exit(1);
        }

        /* write N bytes with stride 'stride' */
        for (int i=0; i<N; ++i) {
            if (lseek(fd, i*stride, SEEK_SET) == (off_t)-1) {
                perror("lseek failed");
                exit(1);
            }
            if (write(fd, "x", 1) == -1) {
                perror("write failed");
                exit(1);
            }
        }
	if (fsync(fd) == -1) {
		perror("fsync failed");
		exit(1);
	}
        if (close(fd) == -1) {
            perror("close failed");
            exit(1);
        }
    }

    return 0;
}
